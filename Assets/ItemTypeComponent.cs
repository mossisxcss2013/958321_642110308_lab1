using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sarawut.Gamedev3.Chapter1
{
    public class ItemTypeComponent : MonoBehaviour
    {
        [SerializeField] 
        protected ItemType m_itemType;

        public ItemType Type
        {
            get { return m_itemType; }
            set { m_itemType = value; }
        }
    }
}

